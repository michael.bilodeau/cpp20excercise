#include <iostream>
#include <ranges>
#include <string>
#include <vector>

struct Product
{
	std::string name_;
	float price_;

	void print() const
	{
		std::cout << name_ << " " << price_ << "$\n";
	}
};

using Products = std::vector<Product>;

Products getProducts()
{
	return {
		{"Orange", 1.5f},
		{"Banana", 2.0f},
		{"Pear", 2.5f},
		{"Grape", 5.5f},
		{"Apple", 1.0f},
		{"Olive", 3.0f},
		{"Peanut", 3.5f},
		{"Chick Pea", 4.5f},
	};
}

void printProductsInAlphabeticalOrder(const Products& products)
{
	std::cout << "printProductsInAlphabeticalOrder: \n";
	// TODO: print all products in alphabetical order
	std::cout << "\n";
}

void printProductsInPriceRange(const Products& products, float lower, float higher)
{
	std::cout << "printProductsInPriceRange: \n";
	// TODO: print all products in the given price range
	std::cout << "\n";
}

void printProductsWithPriceInCents(const Products& products)
{
	std::cout << "printProductsWithPriceInCents: \n";
	// TODO: print all products with price in cents
	std::cout << "\n";
}

void printProductsAllOfTheAbove(const Products& products, float lower, float higher)
{
	std::cout << "printProductsAllOfTheAbove: \n";
	// TODO: print all products in the given price range in alphabetical order with price in cents
	std::cout << "\n";
}

int main()
{
	std::cout << "Hello ranges!\n";

	const auto products = getProducts();

	printProductsInAlphabeticalOrder(products);
	printProductsInPriceRange(products, 2.0f, 3.5f);
	printProductsWithPriceInCents(products);
	printProductsAllOfTheAbove(products, 2.0f, 3.5f);
}
